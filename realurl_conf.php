<?php

$sDomain = $_SERVER['SERVER_NAME'];

$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'].= ',tx_realurl_pathsegment';
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = [
    '_DEFAULT' => [
        'init' => [
            'enableCHashCache' => 1,
            'appendMissingSlash' => 'ifNotFile,redirect',
            'emptyUrlReturnValue' => '/',
            'enableUrlDecodeCache' => 1,
            'enableUrlEncodeCache' => 1,
            'respectSimulateStaticURLs'	=>	0,
            'postVarSet_failureMode' => '',
        ],
        'redirects' => [],
        'preVars' => [
            [
                'GETvar' => 'no_cache',
                'valueMap' => [
                    'nc' => 0,
                    'no_cache' => 0,
                ],
                'noMatch' => 'bypass',
            ],
            [
                'GETvar' => 'L',
                'noMatch' => 'bypass',
            ],
        ],
        'pagePath' => [
            'type' => 'user',
            'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
            'spaceCharacter' => '-',
            'languageGetVar' => 'L',
            'expireDays' => 3,
            'rootpage_id' => 2,
            'firstHitPathCache' => 1,
        ],
        'fixedPostVars' => [
            'newsDetailConfiguration' => [
                [
                    'GETvar' => 'tx_news_pi1[action]',
                    'valueMap' => [
                        'detail' => '',
                    ],
                    'noMatch' => 'bypass'
                ],
                [
                    'GETvar' => 'tx_news_pi1[controller]',
                    'valueMap' => [
                        'News' => '',
                    ],
                    'noMatch' => 'bypass'
                ],
                [
                    'GETvar' => 'tx_news_pi1[news]',
                    'lookUpTable' => [
                        'table' => 'tx_news_domain_model_news',
                        'id_field' => 'uid',
                        'alias_field' => 'IF(path_segment!="", path_segment, title)',
                        'addWhereClause' => ' AND NOT deleted',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-'
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                        'autoUpdate' => 1,
                        'expireDays' => 180,
                        'enable404forInvalidAlias' => 1
                    ]
                ]
            ],
            'newsCategoryConfiguration' => [
                [
                    'GETvar' => 'tx_news_pi1[overwriteDemand][categories]',
                    'lookUpTable' => [
                        'table' => 'tx_news_domain_model_category',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause' => ' AND NOT deleted',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-'
                        ]
                    ]
                ]
            ],
            'newsTagConfiguration' => [
                [
                    'GETvar' => 'tx_news_pi1[overwriteDemand][tags]',
                    'lookUpTable' => [
                        'table' => 'tx_news_domain_model_tag',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'addWhereClause' => ' AND NOT deleted',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-'
                        ]
                    ]
                ]
            ],
        ],
        'postVarSets' => [
            '_DEFAULT' => [
                'controller' => [
                    [
                        'GETvar' => 'tx_news_pi1[action]',
                        'noMatch' => 'bypass'
                    ],
                    [
                        'GETvar' => 'tx_news_pi1[controller]',
                        'noMatch' => 'bypass'
                    ]
                ],
                'dateFilter' => [
                    [
                        'GETvar' => 'tx_news_pi1[overwriteDemand][year]'
                    ],
                    [
                        'GETvar' => 'tx_news_pi1[overwriteDemand][month]'
                    ]
                ],
                'page' => [
                    [
                        'GETvar' => 'tx_news_pi1[@widget_0][currentPage]'
                    ]
                ],
            ],
        ],
        'fileName' => [
            'defaultToHTMLsuffixOnPrev' => 0,
            'index' => [
                'print' => [
                    'keyValues' => [
                        'type' => 98,
                    ],
                ],
                'rss.xml' => [
                    'keyValues' => [
                        'type' => 100,
                    ],
                ],
                'rss091.xml' => [
                    'keyValues' => [
                        'type' => 101,
                    ],
                ],
                'rdf.xml' => [
                    'keyValues' => [
                        'type' => 102,
                    ],
                ],
                'atom.xml' => [
                    'keyValues' => [
                        'type' => 103,
                    ],
                ],
                'sitemap.xml' => [
                    'keyValues' => [
                        'type' => 841132,
                    ],
                ],
                'sitemap.txt' => [
                    'keyValues' => [
                        'type' => 841131,
                    ],
                ],
                'robots.txt' => [
                    'keyValues' => [
                        'type' => 841133,
                    ],
                ],
                '_DEFAULT' => [
                    'keyValues' => [
                        'type' => 0,
                    ]
                ],
            ],
        ],
    ]
];


$sDomain = $_SERVER['SERVER_NAME'];
if (preg_match('/development\.localhost/i', $sDomain)) {
    // domain :: [ROOT]
    $iRootPageId = '2';
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$sDomain] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT'];
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$sDomain]['pagePath']['rootpage_id'] = $iRootPageId;

    // tx_news :: [DETAIL] pids
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$sDomain]['fixedPostVars']['9'] = 'newsDetailConfiguration';
}