plugin.metaseo {

    metaTags.copyright = <MUSTERFIRMA>, %YEAR%, <Alle Rechte vorbehalten.>
    metaTags.email = muster@musterfirma.tld
    metaTags.author = <MUSTERFIRMA>
    metaTags.publisher = <MUSTERFIRMA>
    metaTags.language = German=de

    metaTags.geoPositionLatitude = 48.178605
    metaTags.geoPositionLongitude = 8.616138
    metaTags.geoRegion = DE-BW
    metaTags.geoPlacename = <MUSTERSTADT>

    metaTags.googleVerification =

    services.googleAnalytics = UA-XXXXXXXX-1
    # services.googleAnalytics.domainName =
    # services.googleAnalytics.trackDownloads = 0

    pageTitle.sitetitleGlue = ::
    pageTitle.sitetitle = <MUSTERWEBSEITE-TITEL>

}