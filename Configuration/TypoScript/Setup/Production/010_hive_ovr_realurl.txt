##
## Default
##
config.sys_language_mode = content_fallback
config.sys_language_overlay = 1

config {
  sys_language_uid = 0
  language = en
##
## if /en/ visible in URL
##
# defaultGetVars {
#    L = 0
# }
}

##
## Example configuration for multidomain single tree
##

#[globalVar = GP:L = 1]
#    config {
#      sys_language_uid = 1
#      language = de
#    }
#[global]

#[globalVar = GP:L = 2]
#    config.sys_language_uid = 2
#    config.htmlTag_langKey = de_CH
#    config.language = de-ch
#    config.language_alt = de
#    config.locale_all = de_CH.utf8
#    config.sys_language_mode = content_fallback;1,0
#    #config.sys_language_fallBackOrder=1,0
#[global]

##
## For multiple domains
##
#config {
#    typolinkEnableLinksAcrossDomains = 0
#}