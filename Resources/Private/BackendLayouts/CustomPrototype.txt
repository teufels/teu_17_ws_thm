################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            CustomPrototype {
                title = hive Backend Layout :: Custom :: HTML :: Prototype
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = [1] TEST :: NOT VISIBLE IN FRONTEND
                                        colPos = 1
                                        colspan = 12
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif
            }
        }
    }
}