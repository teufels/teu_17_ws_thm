################################
#### BACKENDLAYOUT: Anchor ####
################################
mod {
    web_layout {
        BackendLayouts {
            CustomAnchor {
                title = hive Backend Layout :: Custom :: HTML :: Anchor
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = [1] Anchor :: TopPage :: NOT VISIBLE IN FRONTEND
                                        colPos = 1
                                        colspan = 12
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:hive_thm_custom/Resources/Public/BackendLayouts/Images/default.gif
            }
        }
    }
}